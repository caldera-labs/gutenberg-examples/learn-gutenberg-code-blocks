<?php

/**
 * Enqueue assets needed for the admin editor.
 *
 * Use the "enqueue_block_assets" action to enqueue
 * assets on the front-end of your website.
 */
function learn_gutenberg_code_blocks_enqueue_block_editor_assets() {
    $dir = dirname( __FILE__ );
    $block_js = '/build/index.js';
    $editor_css = '/build/style.css';
    $handle = 'learn-gutenberg/code-blocks';

    wp_enqueue_script( $handle, plugins_url( $block_js, __FILE__ ), array(
        'wp-blocks',
    ), (filemtime( "$dir/$block_js" ) ));

    wp_enqueue_style( $handle, plugins_url( $editor_css, __FILE__ ), array(
        'wp-blocks',
    ), filemtime( "$dir/$editor_css" ) );
}
add_action( 'enqueue_block_editor_assets', 'learn_gutenberg_code_blocks_enqueue_block_editor_assets' );

