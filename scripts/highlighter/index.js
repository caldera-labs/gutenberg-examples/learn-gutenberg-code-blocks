import { registerBlockType } from "@wordpress/blocks";
//@TODO import from webpack
const __ = wp.i18n.__;
//@todo webpack
const { Component } = wp.element;
const CheckboxControl = wp.blocks.InspectorControls.CheckboxControl;//Native WordPress checkbox control
const SelectControl = wp.blocks.InspectorControls.SelectControl;//Native WordPress select control
import { InspectorControls } from "@wordpress/blocks";
import { PanelBody, PanelRow } from "@wordpress/components";

import Code from '../components/Code'

import ShowCode  from '../components/ShowCode';

const BlockControls = wp.blocks.BlockControls;

import "./style.scss";

registerBlockType( 'learn-gutenberg/code-blocks', {
    title: __( 'Learn Gutenberg Code Blocks', 'learn-gutenberg' ),
    category: 'common',
    // Attributes for accessing configurable block data
    attributes: {
        // The  "codeLeft" value, props.attributes.codeLeft
        codeLeft: {
            type: 'string', // The attribute's value is an string
            source: 'content', // We are using element content
            selector: '.learn-gutenberg-code-left', // The selected element has a class of "learn-gutenberg-code-left",
            default: '<p>Add Some code</p>' //default value
        },
        // The  "codeRight" value, props.attributes.codeRight
        codeRight: {
            type: 'string', // The attribute's value is an string
            source: 'content', // We are using element content
            selector: '.learn-gutenberg-code-right', // The selected element has a class of "learn-gutenberg-code-right",
            default: '<p>Add Some More</p>' //default value
        },
        leftLang: {
            type: 'string', // The attribute's value is an string
        },
        rightLang: {
            type: 'string', // The attribute's value is an string
        },
        // The  "enableRight" value, props.attributes.enableRight
        enableRight: {
            type: 'boolean', //type is boolean
            default: false, //default is false
        }
    },
    edit({attributes, setAttributes, className, focus, id}) {
        const changeLeft = function (value) {
            setAttributes({codeLeft: value})
        };
        const changeRight = function (value) {
            setAttributes({codeRight: value})
        };

        const changeRightEnable = function () {
            let value = !attributes.enableRight;
            setAttributes({enableRight: value});
        };

        const changeLeftLang = (lang) => {
            setAttributes({langLeft: lang});
        };

        const changeRightLang = (lang) => {
            setAttributes({rightLang: lang});
        };

        const languages = [
            {value: 'javascript', label:'JavaScript' },
            {value: 'css', label:'CSS' },
            {value: 'html', label:'HTML' },
        ];

        return (
            <div className={className}>
                {focus &&

                    <InspectorControls>
                        <PanelBody>
                            <PanelRow>
                                <CheckboxControl
                                    instanceId={`${id}-enable-right`}
                                    label="Enable Second Editor"
                                    checked={ attributes.enableRight }
                                    onChange={ changeRightEnable }
                                />
                            </PanelRow>
                            <PanelRow>
                                <SelectControl
                                    label={ __( 'Language One' ) }
                                    value={ attributes.leftLang }
                                    options={languages}
                                    onChange={ changeLeftLang }
                                />
                            </PanelRow>
                            <PanelRow>
                                {attributes.enableRight &&
                                    <SelectControl
                                        label={ __( 'Language Two' ) }
                                        value={ attributes.rightLang }
                                        options={languages}
                                        onChange={ changeRightLang }
                                    />
                                }
                            </PanelRow>
                        </PanelBody>
                    </InspectorControls>

                }
                <Code
                    value={attributes.codeLeft}
                    language={attributes.leftLang}
                    onChange={changeLeft}
                    focus={focus}
                    label={__( 'Code Block One')}
                />
                {attributes.enableRight &&
                    <Code
                        value={attributes.codeRight}
                        language={attributes.rightLang}
                        onChange={changeRight}
                        focus={focus}
                        label={__( 'Code Block Two')}
                    />

                }
            </div>
        );
    },

    save({attributes,className}) {
        return(
            <div className={className}>
                {ShowCode(attributes)}
            </div>
        )
    }
} );
