import { Component } from "@wordpress/element"; //Extend WordPress component instead of React?
import { InspectorControls } from "@wordpress/blocks";//Inspector controls has inputs/selects we can reuse

const CheckboxControl = InspectorControls.CheckboxControl;//Native WordPress checkbox control
const SelectControl =   InspectorControls.SelectControl;//Native WordPress select control
const TextControl =   InspectorControls.TextControl;//Native WordPress select control

//@TODO import from webpack
const __ = wp.i18n.__;


class  ExampleEdit extends Component {

    constructor(props){
        super(props);
        this.state = {
            className: `learn-gutenberg-example-edit learn-gutenberg-example-edit-${props.i}`,
            heading: `${__('Example')} ${props.i}`
        }
    }

    render(){
        return(
            <div
                className={this.state.className}
            >
                <TextControl
                    label={ __( 'Url', 'learn-gutenberg' ) }
                    type={'url'}
                    value={ this.props.url }
                    instanceId={`learn-gutenberg-example-edit-url`}
                    onChange={this.props.onUrlChange}
                />

                <TextControl
                    label={ __( 'Title', 'learn-gutenberg' ) }
                    type={'text'}
                    value={ this.props.title }
                    instanceId={`learn-gutenberg-example-edit-title`}
                    onChange={this.props.onTitleChange}
                />

                <CheckboxControl
                    label={__( 'a11y?', 'learn-gutenberg' )}
                    help={__( 'Is a11y Review Complete?', 'learn-gutenberg' )}
                    checked={ this.props.a11y }
                    onChange={ this.props.onA11yChange }
                />

                <CheckboxControl
                    label={__( 'Security?', 'learn-gutenberg' )}
                    help={__( 'Is Security Review Complete?', 'learn-gutenberg' )}
                    checked={ this.props.security }
                    onChange={ this.props.onSecurityChange }
                />
            </div>
        )
    }

}

export default ExampleEdit;