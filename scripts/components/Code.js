//@todo webpack
const { Component } = wp.element;
const { __ } = wp.i18n;
const TextAreaControl = wp.blocks.InspectorControls.TextareaControl;//Native WordPress textarea control
import brace from 'brace';
import AceEditor from 'react-ace';

import 'brace/mode/javascript';
import 'brace/mode/php';
import 'brace/mode/html';
import 'brace/mode/css';
import 'brace/mode/json';
import 'brace/theme/github';

import { BlockControls } from "@wordpress/blocks";
import { withInstanceId, InspectorControls } from "@wordpress/components";
import HighlightedCode from  './HighlightedCode';
import ShowCode  from '../components/ShowCode';



class Code extends Component {

    constructor(props){
        super(props);
        this.changeCode = this.changeCode.bind(this);
    }

    //Change handler for textarea controlling code
    changeCode(newValue){
        this.props.onChange( newValue );
    }

    render () {
        return (
            <div className="learn-gutenberg-code">
                {! this.props.focus &&
                    <HighlightedCode
                        language={this.props.language || 'html'}
                        value={this.props.value}
                    />
                }
                { this.props.focus &&
                    <div className="learn-gutenberg-code-edit">
                        <AceEditor
                            mode={this.props.language || 'html'}
                            theme={"github"}
                            onChange={this.changeCode}
                            name={`${this.props.id}-editor` }
                            editorProps={{$blockScrolling: true}}
                            value={this.props.value}
                        />
                    </div>
                }

            </div>
        );
    }
}

export default Code;


