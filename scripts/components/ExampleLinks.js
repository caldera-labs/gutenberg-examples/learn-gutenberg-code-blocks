import ExampleLink from './ExampleLink';
//@todo webpack
const el = wp.element.createElement;
const ExampleLinks = (examples) => {
    let els = [];
    examples.forEach( (example, i) => {
       els.push(el('div', {}, ExampleLink(example, i)));
    });
    return el(
        'div',
        {},
        els
    )
};

export default ExampleLinks;