/**
 * Component to show highlighted code
 * @param attributes
 * @returns {XML}
 * @constructor
 */
const ShowCode =  (attributes) =>{
    return (
        <div>
            <pre
                className={`learn-gutenberg-code-left learn-gutenberg-code ${attributes.leftLang}`}
                language={attributes.leftLang}
            >
                    {attributes.codeLeft}
            </pre>
                {attributes.enableRight &&
            <pre
                className={`learn-gutenberg-code-right learn-gutenberg-code ${attributes.rightLang}`}
                language={attributes.rightLang}
            >
                {attributes.codeRight}
            </pre>

            }

        </div>
    )
};

export default ShowCode;