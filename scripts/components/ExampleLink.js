//@todo webpack
const el = wp.element.createElement;
const __ = wp.i18n.__;


const ExampleLink = (props) => {
    console.log(props);
    return el(
        'div',
        {
            className: 'learn-gutenberg-example'
        },
        [
            el(
                'a',
                {
                    className:'learn-gutenberg-example-link',
                    href:props.href
                },
                props.title
            ),
            el(
                'span',
                {
                    className:'learn-gutenberg-example-a11y',
                },
                props.a11y ? __( 'Accessibility Review Completed' ) : __( 'Accessibility Review Pending' )
            ),
            el(
                'span',
                {
                    className:'learn-gutenberg-example-security',
                },
                props.security ? __( 'Security Review Completed' ) : __( 'Security Review Pending' )
            )
        ]

    )
};

export default ExampleLink;