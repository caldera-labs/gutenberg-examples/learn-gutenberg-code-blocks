//@todo webpack
const {Component} = wp.element;


import SyntaxHighlighter from 'react-syntax-highlighter';
import {docco} from 'react-syntax-highlighter/styles/hljs';

class HighlightedCode extends Component {

    render() {
        return (
            <div className="learn-gutenberg-code-highlight">
                <SyntaxHighlighter
                    language={this.props.lang || 'html'}
                    style={docco}
                >
                    {this.props.value}
                </SyntaxHighlighter>
            </div>


        );
    }
}


export default HighlightedCode;