<?php

/**
 * Enqueue assets needed for the front-end rendering
 *
 * Use the "wp_enqueue_scripts" action to enqueue
 * assets on the front-end of your website.
 */
function learn_gutenberg_code_blocks_enqueue_block_assets() {
    $dir = dirname( __FILE__ );
    $block_js = '/index.js';
    $editor_css = '/style.css';
    $handle = 'learn-gutenberg/code-blocks-front';

    //Load highlighter plugin
    wp_enqueue_script( $handle . 'highlight', plugins_url( '/highlight.pack.js', __FILE__ ), array(), (filemtime( "$dir/$block_js" ) ));

    //Load front-end JS to init highlighter
    wp_enqueue_script( $handle, plugins_url( $block_js, __FILE__ ), array(
        $handle . 'highlight'
    ), (filemtime( "$dir/$block_js" ) ));

    //Load github style
    wp_enqueue_style( $handle, plugins_url( '/github.css', __FILE__ ) );
    
}
add_action( 'wp_enqueue_scripts', 'learn_gutenberg_code_blocks_enqueue_block_assets' );

