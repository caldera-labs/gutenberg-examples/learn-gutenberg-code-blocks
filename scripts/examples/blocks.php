<?php

/**
 * Enqueue assets needed for the admin editor.
 *
 * Use the "enqueue_block_assets" action to enqueue
 * assets on the front-end of your website.
 */
function learn_gutenberg_code_blocks_enqueue_block_editor_assets_examples_block() {
    $dir = dirname( __FILE__ );
    $block_js = '/build/index.js';
    $editor_css = '/build/style.css';
    $handle = 'learn-gutenberg/examples-block';

    wp_enqueue_script( $handle, plugins_url( $block_js, __FILE__ ), array(
        'wp-blocks',
    ), (filemtime( "$dir/$block_js" ) ));

    wp_enqueue_style( $handle, plugins_url( $editor_css, __FILE__ ), array(
        'wp-blocks',
    ), filemtime( "$dir/$editor_css" ) );
}
add_action( 'enqueue_block_editor_assets', 'learn_gutenberg_code_blocks_register_example_block' );

add_action( 'plugins_loaded', 'learn_gutenberg_code_blocks_register_example_block' );
function learn_gutenberg_code_blocks_register_example_block(){
    $name = 'learn-gutenberg/examples-block';
    $attributes = array(
        'url' => array(
            'type' => 'string'
        ),
        'title' => array(
            'type' => 'string'
        ),
        'a11y' => array(
            'type' => 'boolean'
        ),
        'security' => array(
            'type' => 'boolean'
        )
    );
    if( WP_Block_Type_Registry::get_instance()->is_registered( $name ) ){
        $block = WP_Block_Type_Registry::get_instance()->get_registered($name);
        if (! $block->attributes) {
            $block->attributes = $attributes;
        }

    }else{
        register_block_type( $name, array(
            'render_callback' => 'learn_gutenberg_code_blocks_register_example_block_render',
            'attributes' => $attributes,
            'title'=> __('Examples Block', 'learn-gutenberg')
        ));
    }

}

function learn_gutenberg_code_blocks_register_example_block_render($attributes){
    return '';
}