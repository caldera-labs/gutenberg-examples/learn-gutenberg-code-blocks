import {registerBlockType} from "@wordpress/blocks";
//@TODO import from webpack
const __ = wp.i18n.__;
const el = wp.element.createElement;
const {Component} = wp.element;

import {InspectorControls} from "@wordpress/blocks";
import {PanelBody, PanelRow, Button, Dashicon} from "@wordpress/components";


import ExampleEdit from '../components/ExampleEdit';
import ExampleLink from '../components/ExampleLink';

const BlockControls = wp.blocks.BlockControls;

import "./style.scss";

registerBlockType('learn-gutenberg/examples-block', {
    title: __( 'Examples Block', 'learn-gutenberg'),
    category: 'common',
    // Attributes for accessing configurable block data
    attributes: {
        url: {
            type: 'string',
        },
        title: {
            type: 'string',
        },
        a11y: {
            type: 'boolean',
        },
        security: {
            type: 'boolean'
        }

    },
    edit({attributes, setAttributes, className, focus, id}) {

        return (
            <div className={className}>
                {focus &&
                <InspectorControls>
                    <PanelBody>
                        <PanelRow>
                            <p>Block Controls!</p>
                        </PanelRow>
                    </PanelBody>
                </InspectorControls>
                }
                <div>
                    <ExampleEdit
                        url={attributes.url}
                        onUrlChange={(newValue) => {
                            setAttributes({url: newValue});
                        }}
                        title={attributes.title}
                        onTitleChange={(newValue) => {
                            setAttributes({title: newValue});
                        }}
                        a11y={attributes.a11y}
                        onA11yChange={() => {
                            let newValue = !attributes.a11y;
                            setAttributes({a11y: newValue});
                        }}
                        security={attributes.security}
                        onSecurityChange={() => {
                            let newValue = !attributes.security;
                            setAttributes({security: newValue});
                        }}
                    />


                </div>

            </div>
        );
    },

    save: function (attributes) {
        return null;
    }
});
