<?php
/**
Plugin Name: Learn Gutenberg Code Blocks
Description: Blocks for learn Gutenberg site related to showing example code.
Version: 0.2.0
 */

/** Syntax Highlight block */
require_once dirname( __FILE__ ) . '/scripts/highlighter/blocks.php';
/** Code Examples Block */
//require_once dirname( __FILE__ ) . '/scripts/examples/blocks.php';
/** Front-end CSS/JS load */
require_once dirname( __FILE__ ) . '/scripts/front/front.php';
