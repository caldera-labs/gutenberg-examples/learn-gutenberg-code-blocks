# Code Blocks

These are blocks related to displaying example code on the Learn Gutenberg site.


## Development
* Clone
    * `git clone git@gitlab.com:caldera-labs/gutenberg-examples/learn-gutenberg-code-blocks.git`
* Install
    * `cd learn-gutenberg-code-blocks && npm install`
* Activate Plugin
    * `wp plugin activate learn-gutenberg-code-blocks`
* Run Webpack Watch
    * `npm run dev`
    
### Build Zip File
`bash ./bin/build-plugin-zip.sh`

## Syntax Highlighter Block
This block provides one or two syntax highlighter blocks.

This is a static block. It saves the code to highlight as static HTML.


### Usage
* Add block
* Click block's "Advanced Options" to open settings in sidebar
* Select language 1
* Put code in code block one
* (Optional) Select to add a second code block
* (Optional) Select language for second code block
* (Optional) Put code in second code block

### Development
* block is in /scripts/highlighter/index.js
* Most code is in /scripts/components
* Front-end CSS is in /scripts/front/style.scss, which compiles to /scripts/front/build/style.css
 

## Example Code Block
This block will list each module's example code and status of that.

This block is not created yet.
