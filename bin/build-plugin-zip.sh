#!/bin/bash

# Exit if any command fails
set -e

# Change to the expected directory
cd "$(dirname "$0")"
cd ..

# Enable nicer messaging for build status
YELLOW_BOLD='\033[1;33m';
COLOR_RESET='\033[0m';
status () {
	echo -e "\n${YELLOW_BOLD}$1${COLOR_RESET}\n"
}

# Run the build
status "Begin build"
status "Installing dependencies..."
npm install
status "Generating build..."
npm run build


# Remove any existing zip file
rm -f learn-gutenberg-code-blocks.zip

# Generate the plugin zip file
status "Creating archive..."
zip -r learn-gutenberg-code-blocks.zip \
	learn-gutenberg-code-blocks.php \
	README.md \
	scripts/*.php \
	scripts/highlighter/build/*.{js,map,php,css} \
	scripts/highlighter/blocks.php \
	scripts/front/build/*.{js,map,php,css} \
	scripts/front/front.php \
status "Done."
